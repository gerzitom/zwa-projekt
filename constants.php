<?php
/**
 * User: tomasgerzicak
 * Date: 03/12/2019
 */





define('PAGE', isset($_GET['page']) ? $_GET['page'] : '');
define('USER', isset($_GET['user']) ? $_GET['user'] : '');

if(isset($_SESSION['logged_user'])){
    define('PROFILE_PICTURE_PATH', './profile_pictures/'.$_SESSION['logged_user']);
}

define('HOME_URL', "http://localhost:8888/zwa_insta");