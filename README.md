# Boilerplate

Here are starting files for my project

## Installation

Clone whole project into your repository and run

```bash
npm i
```

node modules will be installed and you are ready to develop.

## Configuration
You can modify all paths for gulpfile in `gulp_settings/paths.json`

## Usage

Just run

```bash
gulp dev
```
to start the environment
 If you do not want to use wordpress files, just work with `index.html` file and `src` and `assets` folders.


## License
[MIT](https://choosealicense.com/licenses/mit/)