<?php
/**
 * User: tomasgerzicak
 * Date: 02/12/2019
 */


include_once '../db.php';
include_once '../constants.php';

$errorMessage = [];

if(isset($_GET['register'])){
//    var_export($_GET);

    $error = false;

    $formFields = ['name', 'username', 'mail', 'password', 'password-again'];

    // check empty fields
    foreach ($formFields as $formField){
        if(empty($_GET[$formField])){
            $error = true;
            $errorMessage[$formField] = 'This field is required';
        }
    }

    // check if user already exists
    $query = $conn->prepare('SELECT username FROM users WHERE username = ?');
    $query->execute([$_GET['username']]);
    $userExists = $query->fetch();
    var_export($userExists);
//    if($userExists) die

    die();

    // check same passwords
    if($_GET['password'] != $_GET['password-again']){
        $error = true;
        $errorMessage['password'] = $errorMessage['password-again'] = 'Passwords do not match';
    }

    if(!$error){
        //register
        $hash = password_hash($_GET['password'], PASSWORD_DEFAULT);
        try{
            $query = $conn->prepare('INSERT into users (name, username, mail, password) VALUES(?, ?, ?, ?)');
            // add user to database
            $query->execute([$_GET['name'], $_GET['username'], $_GET['mail'], $hash]);
            //create directory for user images
            mkdir('../user_photos/'.$_GET['username'], 0777, true);
            // go to user profile page
            header("Location: ".HOME_URL."?page=user_profile");
        } catch (Exception $e){
            echo "Something went wrong with the database";
            echo $e;
        }
//        continue to dashboard
    }

}

