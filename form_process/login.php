<?php
/**
 * User: tomasgerzicak
 * Date: 03/12/2019
 */

include_once '../db.php';
include_once '../constants.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_GET['login'])){
    $query = $conn->prepare("SELECT id, username, password FROM users");
    $query->execute();
    $users = $query->fetchAll();

    foreach ($users as $user) {
        if(password_verify($_GET['password'], $user['password'])){
            // success login
            $_SESSION['logged_user'] = $user['id'];
            header("Location: ".HOME_URL."?page=user_profile");
//            die();
        }
    }

//    password_verify($_GET['password'], )
}