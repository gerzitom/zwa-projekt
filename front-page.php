<?php
/**
 * Template Name: Homepage
 */

$query = $conn->prepare("SELECT id, image_url, likes, user_id FROM posts");
$query->execute();
$posts = $query->fetchAll();

include_once "./header.php";

?>

<div class="container">
    <div class="row">
        <?php foreach ($posts as $post):
            $user = new User($conn, $post['user_id']);

            //calculate likes
            $query = $conn->prepare("SELECT COUNT(id) FROM likes WHERE post_id = ?");
            $query->execute([$post['id']]);
            $likes = $query->fetch()[0];
            ?>
            <div class="col-md-4">
                <div class="post">
                    <img class="post__image" src="<?= $post['image_url'] ?>" alt="Picture">
                    <p class="post__username"><?= $user->getName() ?></p>
                    <div class="row justify-content-between">
                        <div class="col-auto">
                            <p class="post__likes">Likes: <?= $likes ?></p>
                        </div>
                        <div class="col-auto">
                            <a class="button button--small" href="./?page=user_profile&add_like=<?= $post['id'] ?>">I like it</a>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach; ?>
    </div>
</div>

<?php
include_once "./footer.php";