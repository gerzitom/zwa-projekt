<?php
/**
 * User: tomasgerzicak
 * Date: 03/12/2019
 */

include_once 'db.php';

global $conn;

class User{

    private $name;
    private $username;
    private $mail;
    private $bio;
    private $profilePic;

    /**
     * User constructor.
     * @param PDO $conn
     * @param int $userID ID of the user, if no ID is specified, check, if some user is logged.
     * @throws Exception if no ID specified and no user is logged
     */
    public function __construct($conn, $userID = null){
        $query = $conn->prepare("SELECT username, name, mail, bio, profile_pic FROM users WHERE id = ?");
        if($userID == null){
            if(isset($_SESSION['logged_user'])){
                $userID = $_SESSION['logged_user'];
            } else {
                throw new Exception("No user defined");
            }
        }
        $query->execute([$userID]);
        $data = $query->fetch();
        $this->username = $data['username'];
        $this->name = $data['name'];
        $this->mail = $data['mail'];
        $this->bio = $data['bio'];
        $this->profilePic = $data['profile_pic'];

    }

    static function logIn($userID){
        $_SESSION['logged_user'] = $userID;
    }

    static function logOut(){
        unset($_SESSION['logged_user']);
    }

    static function getLoggedUserID(){
        return isset($_SESSION['logged_user']) ? $_SESSION['logged_user'] : false;
    }

    /**
     * @return string
     */
    public function getProfilePic(){
        return $this->profilePic;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUsername(){
        return $this->username;
    }

    /**
     * @return string
     */
    public function getMail(){
        return $this->mail;
    }

    /**
     * @return string
     */
    public function getBio(){
        return $this->bio;
    }





}