<?php
session_start();

//const SITE = $_GET['page'];

//router


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



include_once 'constants.php';
include_once 'db.php';
include_once 'User.php';
include_once 'Form.php';

if(isset($_GET['logout'])){
    User::logOut();
}


switch (PAGE){
    case 'login':
        include_once 'page/login.php';
        break;
    case 'registration':
        include_once 'page/registration.php';
        break;
    case 'user_profile':
        include_once 'page/user_profile.php';
        break;
    case 'user_settings':
        include_once 'page/user_settings.php';
        break;
    case 'new_post':
        if(isset($_SESSION['logged_user'])){
            include_once 'page/new_post.php';
        } else{
            header("Location: ?page=login");
        }
        break;
    case '':
        if (USER == '') header("Location: ?page=login");
        else {
            include_once 'page/user_profile.php';
        }
        break;
    default:
        include_once 'page/error/404.php';
        break;
}
