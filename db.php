<?php
/**
 * User: tomasgerzicak
 * Date: 02/12/2019
 */

$servername = "localhost";
$username = "root";
$password = "root";
global $conn;
$conn = null;

try {
    $conn = new PDO("mysql:host=$servername;dbname=zwa_dev;charset=utf8mb4", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e)
{
    include_once 'page/error/database_error.php';
    die();
}