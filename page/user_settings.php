<?php
/**
 * User: tomasgerzicak
 * Date: 04/12/2019
 */

if(isset($_POST['save_settings'])){
    $name = htmlspecialchars($_POST['name']);
    $bio = htmlspecialchars($_POST['bio']);
    try{
        $query = $conn->prepare("UPDATE users SET name = ?, bio = ? WHERE id = ?");
        $query->execute([$name, $bio, $_SESSION['logged_user']]);
    } catch (Exception $e){
        header("Location: page/error/database_error.php");
    }
}

// must be after saving settings
$user = new User($conn);

include_once "./header.php";
?>

<div class="user-settings">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <h2><?= $user->getName() ?></h2>
            </div>
            <div class="col-md-10">
                <h1>Settings</h1>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-4">
                            <label for="name">Name</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="name" id="name" value="<?= $user->getName() ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="bio">Bio</label>
                        </div>
                        <div class="col-md-8">
                            <textarea type="text" name="bio" rows="9" id="bio"><?= $user->getBio() ?></textarea>
                        </div>
                    </div>

                    <button class="button" name="save_settings">Save</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php
include_once "./footer.php";