<?php
/**
 * User: tomasgerzicak
 * Date: 30/11/2019
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$errorMessage = [];

if(isset($_POST['login'])) {
    $query = $conn->prepare("SELECT id, username, password FROM users");
    $query->execute();
    $users = $query->fetchAll();

    $userNameExists = false;


    foreach ($users as $user) {
        if ($user['username'] == $_POST['username']) {
            $userNameExists = true;
            //user matches
            if (password_verify($_POST['password'], $user['password'])) {
                //passwords matches
                // success login
                $_SESSION['logged_user'] = $user['id'];
                header("Location: " . HOME_URL . "?page=user_profile");
//            die();
            } else {
                $errorMessage['password'] = "Password is incorrect";
            }
        }
    }
    if(!$userNameExists) $errorMessage['username'] = "Username does not exist";
}

include_once './header.php';

?>


<div class="login">
    <h1>Wellcome to our insta app</h1>
    <form method="post">
        <div class="form-input <?= isset($errorMessage['username']) ? 'form-input--error' : '' ?>">
            <?php if(isset($errorMessage['username'])): ?>
                <p class="form-input__message"><?= $errorMessage['username'] ?></p>
            <?php endif; ?>
            <label for="username">Username</label>
            <input type="text" name="username" id="username" <?= isset($_POST['username']) ? 'value="'.$_POST["username"].'"' : '' ?>>
        </div>

        <div class="form-input <?= isset($errorMessage['password']) ? 'form-input--error' : '' ?>">
            <?php if(isset($errorMessage['password'])): ?>
                <p class="form-input__message"><?= $errorMessage['password'] ?></p>
            <?php endif; ?>
            <label for="password">Password</label>
            <input type="password" name="password" id="password">
        </div>

        <button class="button button--full-width" type="submit" name="login">Login</button>
    </form>
</div>


<?php
include_once './footer.php';
