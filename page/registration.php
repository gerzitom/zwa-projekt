<?php
/**
 * User: tomasgerzicak
 * Date: 02/12/2019
 */

$errorMessage = [];

if(isset($_POST['register'])){

    var_export($_POST);

    $error = false;
    $formFields = ['name', 'username', 'mail', 'password', 'password-again'];

    $username = htmlspecialchars($_POST['username']);
    $name = htmlspecialchars($_POST['name']);
    $mail = htmlspecialchars($_POST['mail']);
    $password = htmlspecialchars($_POST['password']);
    $passwordAgain = htmlspecialchars($_POST['password-again']);

    $profilePicName = "";
    if(isset($_FILES['user_pic'])){
        $profilePicName = $_FILES['user_pic']['name'];
        //TODO same names of pictures
    }


    // check empty fields
    foreach ($formFields as $formField){
        if(empty($_POST[$formField])){
            $error = true;
            $errorMessage[$formField] = 'This field is required';
        }
    }

    // check if user already exists
    $query = $conn->prepare('SELECT username FROM users WHERE username = ?');
    $query->execute([$username]);
    $userExists = $query->fetch();
    if($userExists){
        $errorMessage['username'] = "User already exists";
    } else{
        // check same passwords
        if($password != $passwordAgain){
            $error = true;
            $errorMessage['password'] = $errorMessage['password-again'] = 'Passwords do not match';
        }

        if(!$error){
            //register
            $hash = password_hash($password, PASSWORD_DEFAULT);
            try{

                $query = $conn->prepare('INSERT into users (name, username, mail, password, profile_pic) VALUES(?, ?, ?, ?, ?)');






                // add user to database
//                $query->execute([$name, $username, $mail, $hash, $profilePicName]);
//                $userID = $conn->lastInsertId();

                // login user
//                User::logIn($userID);

                //create directory for user images
//                mkdir('./user_photos/'.$userID, 0777, true);


                // upload user image
                if(isset($_FILES['user_pic'])){
//                    var_export($_FILES);
//                    mkdir("./profile_pictures/".User::getLoggedUserID()."/");
//                    $path = "./profile_pictures/".User::getLoggedUserID()."/".$profilePicName;


                    $path = './profile_pictures/'.$_FILES['user_pic']['name'];

//                    if(!move_uploaded_file($_FILES['user_pic']['tmp_name'], $path)){
//                        $errorMessage['profile_pic'] = "Error uploading profile pic";
//                    }
                }

                // go to user profile page
//                header("Location: ".HOME_URL."?page=user_profile");
            } catch (Exception $e){
                echo "Something went wrong with the database";
                echo $e;
            }
//        continue to dashboard
        }
    }

}


include_once './header.php';

?>

<div class="registration">
    <div class="registration__content">
        <h1>Registration</h1>


        <form action="" class="form" method="post" enctype="multipart/form-data">

            <input type="hidden" name="page" value="registration">

            <div class="form-input">
                <?php if(isset($errorMessage['profile_pic'])): ?>
                    <p class="form-input__message"><?= $errorMessage['profile_pic'] ?></p>
                <?php endif; ?>
                <label for="pic">Profile picture</label>
                <input type="file" accept="image/*" name="user_pic" id="pic">
            </div>


            <div class="form-input">
                <label for="name">Full name</label>
                <input type="text" name="name" id="name" autocomplete="name" <?= isset($_POST['name']) ? 'value="'.$_POST["name"].'"' : '' ?>>
            </div>

            <div class="form-input">
                <?php if(isset($errorMessage['username'])): ?>
                    <p class="form-input__message"><?= $errorMessage['username'] ?></p>
                <?php endif; ?>
                <label for="username">Username</label>
                <input type="text" name="username" id="username" autocomplete="username" <?= isset($_POST['username']) ? 'value="'.$_POST["username"].'"' : '' ?>>
            </div>

            <div class="form-input">
                <?php if(isset($errorMessage['mail'])): ?>
                    <p class="form-input__message"><?= $errorMessage['mail'] ?></p>
                <?php endif; ?>
                <label for="mail">E-mail</label>
                <input type="email" name="mail" id="mail" <?= isset($_POST['mail']) ? 'value="'.$_POST["mail"].'"' : '' ?>>
            </div>

            <div class="form-input <?= isset($errorMessage['password']) ? 'form-input--error' : '' ?>">
                <?php if(isset($errorMessage['password'])): ?>
                    <p class="form-input__message"><?= $errorMessage['password'] ?></p>
                <?php endif; ?>
                <label for="password">Password</label>
                <input type="password" name="password" id="password">
            </div>

            <div class="form-input <?= isset($errorMessage['password-again']) ? 'form-input--error' : '' ?>">
                <?php if(isset($errorMessage['password-again'])): ?>
                    <p class="form-input__message"><?= $errorMessage['password-again'] ?></p>
                <?php endif; ?>
                <label for="password-again">Password again</label>
                <input type="password" name="password-again" id="password-again">
            </div>

            <button class="button button--full-width" type="submit" name="register" value="true">Register</button>
        </form>
    </div>
</div>


<!--<script>-->
<!--    const loginForm = document.querySelector(".registration form");-->
<!--    loginForm.addEventListener("submit", function (ev) {-->
<!--        const inputs = document.querySelectorAll(".registration form input:not([type='hidden'])");-->
<!--        Array.from(inputs).forEach(input => {-->
<!--            if(input.value == ""){-->
<!--                console.log(input);-->
<!--                addErrorMessage("This field is required", input)-->
<!--            }-->
<!--        });-->
<!--        ev.preventDefault();-->
<!--    })-->
<!---->
<!--    function addErrorMessage(message, element) {-->
<!---->
<!--        element.parentElement.classList.add("form-input--error");-->
<!---->
<!--        let errorMessageExists = false;-->
<!--        let errorMessage = null;-->
<!--        Array.from(element.parentElement.children).forEach(child => {-->
<!--            if(child.classList.contains("form-input__message")){-->
<!--                errorMessageExists = true;-->
<!--                errorMessage = child-->
<!--            }-->
<!--        });-->
<!--        if(!errorMessageExists){-->
<!--            errorMessage = document.createElement("DIV");-->
<!--            errorMessage.classList.add("form-input__message");-->
<!--        }-->
<!--        errorMessage.html = message;-->
<!--        element.parentElement.appendChild(errorMessage);-->
<!--    }-->
<!--</script>-->

<?php
include_once './footer.php';
?>
