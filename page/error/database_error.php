<?php
/**
 * User: tomasgerzicak
 * Date: 03/12/2019
 */
?>

<div class="error">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <h1>Something went wrong with the database</h1>
                <p>Sorry</p>
                <?php
                echo "Connection failed: " . $e->getMessage();
                ?>
            </div>
        </div>
    </div>
</div>