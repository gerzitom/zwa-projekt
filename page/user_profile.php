<?php
/**
 * User: tomasgerzicak
 * Date: 02/12/2019
 */
include_once "./header.php";


if(isset($_GET['add_like'])){
    $query = $conn->prepare("SELECT id FROM likes WHERE user_id = ? AND post_id = ?");
    $query->execute([$_SESSION['logged_user'],$_GET['add_like']]);
    $likeExist = $query->fetch();
    if(!$likeExist){
        $query = $conn->prepare("INSERT INTO likes (user_id, post_id) VALUES (?, ?)");
        $query->execute([$_SESSION['logged_user'],$_GET['add_like']]);
    }
}

//get posts
$query = $conn->prepare("SELECT id, image_url, likes, user_id FROM posts");
$query->execute();
$posts = $query->fetchAll();

?>

<div class="user-profile">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-3">
                <?php
                $query = $conn->prepare("SELECT name, bio FROM users WHERE id = ?");
                $query->execute([$_SESSION['logged_user']]);
                $userInfo = $query->fetch();

                $loggedUser = new User($conn);


                ?>
                <img class="user__profile-pic" src="<?= PROFILE_PICTURE_PATH."/".$loggedUser->getProfilePic() ?>" alt="">
                <div class="user__stats">
                    <div class="user__stat">
                        <p>20</p>
                        <p>followers</p>
                    </div>
                    <div class="user__stat">
                        <p>20</p>
                        <p>posts</p>
                    </div>
                    <div class="user__stat">
                        <p>20</p>
                        <p>folloving</p>
                    </div>
                </div>
                <div class="row justify-content-between">
                    <div class="col-auto">
                        <p class="user__username"><?= $loggedUser->getUsername() ?></p>
                    </div>
                    <div class="col-auto">
                        <a href="?page=user_settings" class="button button--small">Settings</a>
                    </div>
                </div>
                <h1><?= $userInfo['name'] ?></h1>
                <p><?= $userInfo['bio'] ?></p>
                <a href="?page=new_post" class="button">Add new post</a>

                <a class="button" href="?logout">Logout</a>
            </div>
            <div class="col-sm-9">
                <section class="posts">
                    <div class="row">
                        <?php foreach($posts as $key => $post):
                        $user = new User($conn, $post['user_id']);

                        //calculate likes
                        $query = $conn->prepare("SELECT COUNT(id) FROM likes WHERE post_id = ?");
                        $query->execute([$post['id']]);
                        $likes = $query->fetch()[0];
                        ?>
                        <div class="col-xl-4 col-md-6">
                            <div class="post">
                                <img class="post__image" src="<?= $post['image_url'] ?>" alt="Picture">
                                <p class="post__username"><?= $user->getName() ?></p>
                                <div class="row justify-content-between">
                                    <div class="col-auto">
                                        <p class="post__likes">Likes: <?= $likes ?></p>
                                    </div>
                                    <div class="col-auto">
                                        <a class="button button--small" href="./?page=user_profile&add_like=<?= $post['id'] ?>">I like it</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<?php
include_once "./footer.php";
?>

