<?php
/**
 * User: tomasgerzicak
 * Date: 03/12/2019
 */

if(isset($_POST['post_image'])){
    $path = './user_photos/'.$_SESSION['logged_user']."/".$_FILES['image_to_post']['name'];
    if(move_uploaded_file($_FILES['image_to_post']['tmp_name'], $path)){
        $query = $conn->prepare('INSERT INTO posts (image_url, user_id) VALUES (?, ?)');
        $query->execute([$path, $_SESSION['logged_user']]);
        header("Location: ./?page=user_profile");
    } else{
        echo "Uploading file failed!";
    }
}

include_once "./header.php";
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-7">
            <form class="form" method="POST" enctype="multipart/form-data">
                <input type="text" name="text">
                <input type="file" name="image_to_post">
                <button class="button" name="post_image" type="submit">Upload</button>
            </form>
        </div>
    </div>
</div>

<?php
include_once "./footer.php";